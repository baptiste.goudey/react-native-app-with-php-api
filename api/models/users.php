<?php
class Users
{

    private $conn;
    private $table_name = "users";

    public $id;
    public $prenom;
    public $nom;
    public $email;
    public $telephone;
    public $etat;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    function create()
    {

        $query = "INSERT INTO
                " . $this->table_name . "
            SET
            prenom=:prenom, nom=:nom, email=:email, telephone=:telephone";

        $stmt = $this->conn->prepare($query);

        $this->prenom = htmlspecialchars(strip_tags($this->prenom));
        $this->nom = htmlspecialchars(strip_tags($this->nom));
        $this->email = htmlspecialchars(strip_tags($this->email));
        $this->telephone = htmlspecialchars(strip_tags($this->telephone));

        $stmt->bindParam(":prenom", $this->prenom);
        $stmt->bindParam(":nom", $this->nom);
        $stmt->bindParam(":email", $this->email);
        $stmt->bindParam(":telephone", $this->telephone);

        if ($stmt->execute()) {
            return true;
        }

        return false;
    }
}
