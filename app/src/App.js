import React from 'react';

import {LogBox} from 'react-native';
import HomeScreen from './screens/homeScreen';

LogBox.ignoreAllLogs(true);

const App = () => {
  return <HomeScreen />;
};

export default App;
