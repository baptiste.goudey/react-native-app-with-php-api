import qs from 'query-string';
import {API_URL} from '../configs/AppConfig';

export function request({url, method = 'GET', params = {}}) {
  let body;
  let headers = {};

  if (method === 'GET') {
    const query = qs.stringify(params);
    url = `${url}?${query}`;
  } else {
    body = JSON.stringify(params);
  }

  if (url.indexOf('http') === -1) {
    url = API_URL + url + '.php';
    headers.Accept = 'application/json';
    headers['Content-Type'] = 'application/json';

    if (method !== 'GET') {
      headers['Cache-Control'] = 'no-cache';
    }
  }

  return fetch(url, {
    method,
    headers,
    body,
  })
    .then((data) => ({data}))
    .catch((e) => {
      console.warn(e);
      throw e;
    });
}
