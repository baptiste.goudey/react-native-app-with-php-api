import {request} from '../utils/request';

export function postUser(params) {
  return request({
    url: '/users/create',
    method: 'POST',
    params,
  });
}
