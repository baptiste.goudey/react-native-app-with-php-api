import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import UserForm from '../forms/userForm';

const HomeScreen = () => {
  return (
    <SafeAreaView forceInset={{bottom: 'never'}} style={styles.container}>
      <UserForm />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
});

export default HomeScreen;
