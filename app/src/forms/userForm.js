import {Formik} from 'formik';
import React from 'react';
import * as Yup from 'yup';
import {View, TextInput, Button, Text, StyleSheet} from 'react-native';
import * as UserService from '../services/userService';

const phoneRegex = RegExp(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/);

const validationSchema = Yup.object().shape({
  email: Yup.string().email('Email invalide').required('Email Requis'),
  prenom: Yup.string().required('Prenom Requis'),
  nom: Yup.string().required('Nom Requis'),
  telephone: Yup.string()
    .matches(phoneRegex, 'Telephone invalide')
    .required('Telephone requis'),
});

const UserForm = () => {
  return (
    <Formik
      onSubmit={(values) => {
        UserService.postUser(values);
      }}
      validationSchema={validationSchema}
      initialValues={{}}>
      {({handleChange, handleBlur, handleSubmit, values, errors, touched}) => {
        return (
          <View style={styles.container}>
            <TextInput
              style={styles.inputStyle}
              label={'Prenom'}
              placeholder={'Tapez ici le prenom'}
              name="prenom"
              type="text"
              onChangeText={handleChange('prenom')}
              onBlur={handleBlur('prenom')}
              value={values.prenom}
            />
            {touched.prenom && errors.prenom && <Text>{errors.prenom}</Text>}
            <TextInput
              style={styles.inputStyle}
              label={'Nom'}
              placeholder={'Tapez ici le nom'}
              name="nom"
              type="text"
              onChangeText={handleChange('nom')}
              onBlur={handleBlur('nom')}
              value={values.nom}
            />
            {touched.nom && errors.nom && <Text>{errors.nom}</Text>}

            <TextInput
              style={styles.inputStyle}
              label={'Email'}
              placeholder={'Tapez ici le mail'}
              name="email"
              type="email"
              onChangeText={handleChange('email')}
              onBlur={handleBlur('email')}
              value={values.email}
            />
            {touched.email && errors.email && <Text>{errors.email}</Text>}

            <TextInput
              style={styles.inputStyle}
              label={'Telephone'}
              placeholder={'Tapez ici votre numero'}
              name="telephone"
              onChangeText={handleChange('telephone')}
              onBlur={handleBlur('telephone')}
              value={values.telephone}
            />
            {touched.telephone && errors.telephone && (
              <Text>{errors.telephone}</Text>
            )}
            <Button onPress={handleSubmit} title="Envoyer" />
          </View>
        );
      }}
    </Formik>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
  inputStyle: {
    borderWidth: 1,
    borderColor: '#4e4e4e',
    padding: 12,
    marginBottom: 5,
  },
});

export default UserForm;
