/**
 * @format
 */

import 'react-native';
import React from 'react';

import {render, fireEvent, waitFor} from '@testing-library/react-native';
import UserForm from '../src/forms/userForm';

test('submits correct values', async () => {
  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve('Default'),
    }),
  );
  const {getByPlaceholderText, getByText} = render(<UserForm />);

  const nom = getByPlaceholderText('Tapez ici le nom');
  const prenom = getByPlaceholderText('Tapez ici le prenom');
  const email = getByPlaceholderText('Tapez ici le mail');
  const tel = getByPlaceholderText('Tapez ici votre numero');
  const submit = getByText('Envoyer');

  await waitFor(() => {
    fireEvent.changeText(tel, '0611486045');
    fireEvent.changeText(prenom, 'mocksurname');
    fireEvent.changeText(email, 'test@test.com');
    fireEvent.changeText(nom, 'mockname');

    expect(nom.props.value).toBe('mockname');
    expect(tel.props.value).toBe('0611486045');
    expect(prenom.props.value).toBe('mocksurname');
    expect(email.props.value).toBe('test@test.com');

    fireEvent.press(submit);

    expect(global.fetch).toHaveBeenCalledWith(
      'http://10.0.2.2/api/users/create.php',
      {
        method: 'POST',
        body: JSON.stringify({
          telephone: '0611486045',
          prenom: 'mocksurname',
          email: 'test@test.com',
          nom: 'mockname',
        }),
        headers: {
          Accept: 'application/json',
          'Cache-Control': 'no-cache',
          'Content-Type': 'application/json',
        },
      },
    );
  });
});
